package ro.alexi.awbd.repository;


import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ro.alexi.awbd.model.Address;

import java.util.Optional;
import java.util.UUID;
//Nu vom utiliza standardul JDBC pentru ca este invechit si vom utiliza JPA.
@Repository
public interface AddressRepository extends JpaRepository<Address, UUID> {

    Page<Address> findAllByCustomerId(UUID customerId, Pageable pageable);

    Optional<Address> findByAddressAndCustomerId(String address, UUID customerId);
}
