package ro.alexi.awbd.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ro.alexi.awbd.model.Product;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface ProductRepository extends JpaRepository<Product, UUID> {

    Optional<Product> findFirstByProductNameAndPriceAndProductSupplierId(String productName, Float price, UUID supplierId);
}
