package ro.alexi.awbd.controller;


import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.alexi.awbd.dtos.ProductDTO;
import ro.alexi.awbd.dtos.paginationDTO.ProductPaginationResponse;
import ro.alexi.awbd.service.ProductService;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/awbd/products")
public class ProductController {

    private final ProductService productService;

    public ProductController(ProductService productService) {
        this.productService = productService;
    }


    @GetMapping("/list")
    public ResponseEntity<List<ProductDTO>> getProducts(@RequestParam int pageNumber, @RequestParam int pageSize) {
        Pageable pageable = PageRequest.of(pageNumber, pageSize);
        ProductPaginationResponse products = productService.getProducts(pageable);

        var headers = new HttpHeaders();
        headers.add("X-Total-Count", String.valueOf(products.getTotalItems()));

        return new ResponseEntity<>(products.getProducts(), headers, HttpStatus.OK);

    }

    @PostMapping("")
    public ResponseEntity<ProductDTO> saveProduct(@RequestBody ProductDTO product) {
        ProductDTO productDTO = productService.saveProduct(product);

        return new ResponseEntity<>(productDTO, HttpStatus.ACCEPTED);
    }

    @DeleteMapping("")
    public void deleteProduct(@RequestParam UUID productId) {
        productService.deleteProduct(productId);
    }
}
