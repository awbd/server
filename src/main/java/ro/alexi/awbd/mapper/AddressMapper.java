package ro.alexi.awbd.mapper;

import org.mapstruct.Mapper;
import ro.alexi.awbd.dtos.AddressDTO;
import ro.alexi.awbd.model.Address;


@Mapper(componentModel = "spring", uses = {})
public interface AddressMapper extends EntityMapper<AddressDTO, Address> {

    Address toEntity (AddressDTO addressDTO);

    AddressDTO toDto (Address address);

}
