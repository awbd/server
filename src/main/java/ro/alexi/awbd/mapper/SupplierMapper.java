package ro.alexi.awbd.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import ro.alexi.awbd.dtos.SupplierDTO;
import ro.alexi.awbd.model.Supplier;

@Mapper(componentModel = "spring", uses = {})
public interface SupplierMapper extends EntityMapper<SupplierDTO, Supplier> {

    @Mapping(target = "id", source = "id")
    Supplier toEntity(SupplierDTO supplierDTO);

    @Mapping(target = "id", source = "id")
    SupplierDTO toDto(Supplier supplier);
}
