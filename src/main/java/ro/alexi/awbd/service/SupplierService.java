package ro.alexi.awbd.service;


import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import ro.alexi.awbd.dtos.ProductDTO;
import ro.alexi.awbd.dtos.SupplierDTO;
import ro.alexi.awbd.dtos.paginationDTO.SupplierPaginationResponse;
import ro.alexi.awbd.dtos.WarehouseDTO;
import ro.alexi.awbd.errors.CustomException;
import ro.alexi.awbd.mapper.ProductMapper;
import ro.alexi.awbd.mapper.SupplierMapper;
import ro.alexi.awbd.mapper.WarehouseMapper;
import ro.alexi.awbd.model.Product;
import ro.alexi.awbd.model.Supplier;
import ro.alexi.awbd.model.Warehouse;
import ro.alexi.awbd.repository.SupplierRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class SupplierService {
    private final SupplierRepository supplierRepository;
    private final SupplierMapper supplierMapper;
    private final WarehouseMapper warehouseMapper;
    private final ProductMapper productMapper;

    public SupplierService(SupplierRepository supplierRepository, SupplierMapper supplierMapper, WarehouseMapper warehouseMapper, ProductMapper productMapper) {
        this.supplierRepository = supplierRepository;
        this.supplierMapper = supplierMapper;
        this.warehouseMapper = warehouseMapper;
        this.productMapper = productMapper;
    }

    public SupplierPaginationResponse getSuppliers(Pageable pageable, boolean completeList) {
        SupplierPaginationResponse supplierPaginationResponse = new SupplierPaginationResponse();
        if (completeList) {
            List<Supplier> all = supplierRepository.findAll();

            supplierPaginationResponse.setTotalItems(all.size());
            supplierPaginationResponse.setSuppliers(supplierMapper.toDto(all));
        } else {
            Page<Supplier> suppliers = supplierRepository.findAll(pageable);

            supplierPaginationResponse.setTotalItems(suppliers.getTotalElements());
            supplierPaginationResponse.setSuppliers(supplierMapper.toDto(suppliers.getContent()));

        }

        return supplierPaginationResponse;
    }

    public SupplierDTO saveSupplier(SupplierDTO supplierDTO) {
        Optional<Supplier> optionalSupplier = supplierRepository.findByNameAndAddress(supplierDTO.getName(), supplierDTO.getAddress());

        if (optionalSupplier.isEmpty() || supplierDTO.getId() != null) {
            Supplier savedSupplier = supplierRepository.save(supplierMapper.toEntity(supplierDTO));
            return supplierMapper.toDto(savedSupplier);
        } else {
            throw new CustomException(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST, "Furnizorul exista deja");
        }
    }

    public void deleteSupplier(UUID supplierId) {
        Optional<Supplier> byId = supplierRepository.findById(supplierId);

        if (byId.isPresent()) {
            supplierRepository.delete(byId.get());
        } else {
            throw new CustomException(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND, "Furnziorul nu a fost gasit");
        }
    }

    public List<WarehouseDTO> findAllWarehousesBySupplierId(UUID parentId) {
        Optional<Supplier> supplierOptional = supplierRepository.findById(parentId);

        if (supplierOptional.isEmpty()) {
            throw new CustomException(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND, "Furnizorul nu a fost gasit");
        }
        Supplier supplier = supplierOptional.get();

        ArrayList<Warehouse> warehouses = new ArrayList<>(supplier.getWarehouses());

        return warehouseMapper.toDto(warehouses);
    }

    public List<ProductDTO> findAllProductsBySupplierId(UUID parentId) {
        Optional<Supplier> supplierOptional = supplierRepository.findById(parentId);

        if (supplierOptional.isEmpty()) {
            throw new CustomException(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND, "Furnizorul nu a fost gasit");
        }
        Supplier supplier = supplierOptional.get();

        ArrayList<Product> products = new ArrayList<>(supplier.getProducts());

        return productMapper.toDto(products);
    }
}
