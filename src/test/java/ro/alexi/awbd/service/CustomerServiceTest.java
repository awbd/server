package ro.alexi.awbd.service;


import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.junit4.SpringRunner;
import ro.alexi.awbd.config.ConfigClass;
import ro.alexi.awbd.dtos.CustomUserDetails;
import ro.alexi.awbd.dtos.CustomerDTO;
import ro.alexi.awbd.errors.CustomException;
import ro.alexi.awbd.jwt.JwtTokenUtil;
import ro.alexi.awbd.mapper.CustomerMapper;
import ro.alexi.awbd.mapper.CustomerMapperImpl;
import ro.alexi.awbd.model.Customer;
import ro.alexi.awbd.repository.CustomerRepository;
import ro.alexi.awbd.security.SecurityUserService;

import java.util.Collections;
import java.util.Optional;
import java.util.UUID;

import static org.junit.Assert.*;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {CustomerMapperImpl.class, JwtTokenUtil.class, ConfigClass.class})
class CustomerServiceTest {
    CustomerService customerService;

    @Mock
    private SecurityContext securityContext;

    @Mock
    CustomerRepository customerRepository;

    @Mock
    SecurityUserService securityUserService;

    @Mock
    AuthenticationManager authenticationManager;

    @Autowired
    JwtTokenUtil jwtTokenUtil;

    @Autowired
    CustomerMapper customerMapper;


    @Autowired
    ConfigClass configClass;

    @BeforeEach
    void setUp() {
        Authentication mockAuth = Mockito.mock(Authentication.class);
        Mockito.when(securityContext.getAuthentication()).thenReturn(mockAuth);
        when(mockAuth.getPrincipal()).thenReturn(customUserDetails());
        SecurityContextHolder.setContext(securityContext);

        when(securityUserService.loadUserByUsername(any())).thenReturn(customUserDetails());
        when(authenticationManager.authenticate(any())).thenReturn(mockAuth);

        customerService = new CustomerService(customerRepository, customerMapper, authenticationManager, jwtTokenUtil, securityUserService, configClass);
    }

    @Test
    void loginAsCustomer() throws Exception {
        CustomerDTO initial = customerDTO();
        when(customerRepository.findByEmail(any())).thenReturn(Optional.of(customer()));
        CustomerDTO after = customerService.loginAsCustomer(initial);

        assertEquals(initial.getEmail(), after.getEmail());
        assertEquals(initial.getPassword(), after.getPassword());
        assertEquals(initial.getName(), after.getName());
    }

    @Test
    @DisplayName("Login - error")
    void loginAsCustomerWithError() {
        CustomerDTO initial = customerDTO();
        when(customerRepository.findByEmail(any())).thenReturn(Optional.empty());


        CustomException exception = assertThrows(CustomException.class, () -> {
            customerService.loginAsCustomer(initial);
        });

        assertEquals(exception.getErrorMessage(), "Clientul nu exista!");
        assertEquals(exception.getHttpStatus(), HttpStatus.BAD_REQUEST);

    }

    @Test
    void registerCustomer() {
        CustomerDTO initial = customerDTO();
        when(customerRepository.findByEmail(any())).thenReturn(Optional.empty());
        when(customerRepository.save(any())).thenReturn(customer());

        CustomerDTO after = customerService.registerCustomer(initial);
        assertEquals(initial.getEmail(), after.getEmail());
        assertTrue(configClass.passwordEncoder().matches(after.getPassword(), initial.getPassword()));
        assertEquals(initial.getName(), after.getName());

    }

    @Test
    @DisplayName("Register - error")
    void registerCustomerWithError() {
        CustomerDTO initial = customerDTO();
        when(customerRepository.findByEmail(any())).thenReturn(Optional.of(customer()));
        CustomException exception = assertThrows(CustomException.class, () -> {
            customerService.registerCustomer(initial);
        });

        assertEquals(exception.getErrorMessage(), "Clientul exista deja!");
        assertEquals(exception.getHttpStatus(), HttpStatus.BAD_REQUEST);

    }

    @Test
    void getLoggedCustomer() {
        Customer initial = customer();
        when(customerRepository.findById(any())).thenReturn(Optional.of(initial));

        CustomerDTO after = customerService.getLoggedCustomer();

        assertEquals(initial.getEmail(), after.getEmail());
        assertEquals(initial.getPassword(), after.getPassword());
        assertEquals(initial.getName(), after.getName());

        when(customerRepository.findById(any())).thenReturn(Optional.empty());
        CustomerDTO after1 = customerService.getLoggedCustomer();
        assertNull(after1);

    }

    private CustomUserDetails customUserDetails() {
        Customer customer = new Customer();
        customer.setEmail("TEST");
        return new CustomUserDetails("TEST", "TEST", Collections.emptyList(), UUID.randomUUID(), customer);
    }

    private Customer customer() {
        Customer customer = new Customer();
        customer.setEmail("TEST_EMAIL");
        customer.setName("TEST_NAME");
        customer.setPassword("TEST_PASSWORD");
        return customer;
    }

    private CustomerDTO customerDTO() {
        CustomerDTO customerDTO = new CustomerDTO();
        customerDTO.setEmail("TEST_EMAIL");
        customerDTO.setName("TEST_NAME");
        customerDTO.setPassword("TEST_PASSWORD");
        return customerDTO;
    }
}